package lux.vet.pet.implementation;

import lux.vet.pet.model.dto.PetDtoAdd;
import lux.vet.pet.model.dto.PetDtoList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
class PetServiceImplTest {

    @Mock
    private PetServiceImpl service;

    public PetDtoAdd petDtoAdd;

    @BeforeEach
    void setUp() {

        List<PetDtoList> list = new ArrayList<>();
        list.add(PetDtoList.builder().id(1L).name("Tom").color("red").sex("male").specie("gato").breed("persa").status("activo").build());
        list.add(PetDtoList.builder().id(2L).name("Tim").color("red").sex("male").specie("perro").breed("pitbull").status("activo").build());

        petDtoAdd = PetDtoAdd.builder().id(1L).name("Tom").color("red").sex("male").specieId(1L).breedId(1L).status("activo").build();

        Mockito.when(service.findAll()).thenReturn(list);
        Mockito.when(service.findById(1L)).thenReturn(petDtoAdd);
        Mockito.when(service.save(petDtoAdd)).thenReturn(petDtoAdd);
        Mockito.when(service.update(1L, petDtoAdd)).thenReturn(petDtoAdd);
        Mockito.when(service.delete(1L)).thenReturn(petDtoAdd);
    }

    @Test
    void find_pets() {
        List<PetDtoList> petDto = service.findAll();
        assertEquals(2, petDto.size());
        assertFalse(petDto.isEmpty());
        assertNotNull(petDto);
    }

    @Test
    void find_pet_by_id() {
        PetDtoAdd pet = service.findById(1L);
        assertEquals(pet.getName(), petDtoAdd.getName());
        assertNotNull(pet);
    }

    @Test
    void save_pet() {
        PetDtoAdd pet = service.save(petDtoAdd);
        assertEquals(pet.getName(), petDtoAdd.getName());
        assertNotNull(pet);
    }

    @Test
    void update_pet() {
        petDtoAdd.setName("pepe");
        PetDtoAdd edit = service.update(1L, petDtoAdd);
        assertEquals(edit.getName(), petDtoAdd.getName());
        assertNotNull(edit);
    }

    @Test
    void delete_pet() {
        petDtoAdd.setStatus("inactivo");
        PetDtoAdd pet = service.delete(1L);
        assertEquals("inactivo", pet.getStatus());
    }
}