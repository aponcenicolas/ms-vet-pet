package lux.vet.pet.implementation;

import lux.vet.pet.model.dto.BreedDtoList;
import lux.vet.pet.model.dto.SpecieDto;
import lux.vet.pet.model.entity.Specie;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@SpringBootTest
class SpecieServiceImplTest {


    @Mock
    private SpecieServiceImpl service;

    @Autowired
    private ModelMapper modelMapper;

    public SpecieDto specieDto;

    private SpecieDto convertToDto(Specie specie) {
        return modelMapper.map(specie, SpecieDto.class);
    }

    private Specie convertToEntity(SpecieDto specieDto) {
        return modelMapper.map(specieDto, Specie.class);
    }

    @BeforeEach
    void setUp() {

        List<SpecieDto> list = new ArrayList<>();
        list.add(SpecieDto.builder().id(1L).name("Gato").status("activo").build());
        list.add(SpecieDto.builder().id(2L).name("Perro").status("activo").build());
        list.add(SpecieDto.builder().id(3L).name("Conejo").status("activo").build());

        specieDto = SpecieDto.builder().id(1L).name("Gato").status("activo").build();


        Mockito.when(service.findAll()).thenReturn(list);
        Mockito.when(service.findById(specieDto.getId())).thenReturn(specieDto);
        Mockito.when(service.save(specieDto)).thenReturn(specieDto);
        Mockito.when(service.update(1L, specieDto)).thenReturn(specieDto);
        Mockito.when(service.delete(1L)).thenReturn(specieDto);

    }

    @Test
    void find_All_species() {
        List<SpecieDto> species = service.findAll();
        assertEquals(3, species.size());
        assertNotNull(species);
    }

    @Test
    void find_by_id_specie() {
        SpecieDto specie = service.findById(1L);
        assertEquals(specie.getName(), specieDto.getName());
        assertNotNull(specie);
    }


    @Test
    void save_specie() {
        SpecieDto save = service.save(specieDto);
        assertEquals(save.getName(), specieDto.getName());
        assertNotNull(save);
    }

    @Test
    void update_specie() {
        specieDto.setName("Perro");
        SpecieDto edit = service.update(1L, specieDto);
        assertEquals(edit.getName(), specieDto.getName());
        assertNotNull(edit);
    }

    @Test
    void delete_specie() {
        specieDto.setStatus("inactivo");
        SpecieDto drop = service.delete(1L);
        assertEquals("inactivo", drop.getStatus());
    }
}