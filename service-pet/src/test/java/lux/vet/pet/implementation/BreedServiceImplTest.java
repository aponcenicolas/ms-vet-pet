package lux.vet.pet.implementation;


import lux.vet.pet.model.dto.BreedDtoAdd;
import lux.vet.pet.model.dto.BreedDtoList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
class BreedServiceImplTest {

    public BreedDtoAdd breedDtoAdd;

    @Mock
    private BreedServiceImpl service;

    @BeforeEach
    void setUp() {


        List<BreedDtoList> list = new ArrayList<>();
        list.add(BreedDtoList.builder().id(1L).name("Persa").specie("Gato").status("activo").build());
        list.add(BreedDtoList.builder().id(2L).name("Angora").specie("Perro").status("activo").build());

        breedDtoAdd = BreedDtoAdd.builder().id(1L).name("Persa").specieId(1L).status("activo").build();

        Mockito.when(service.findAll()).thenReturn(list);
        Mockito.when(service.findById(1L)).thenReturn(breedDtoAdd);
        Mockito.when(service.save(breedDtoAdd)).thenReturn(breedDtoAdd);
        Mockito.when(service.update(1L, breedDtoAdd)).thenReturn(breedDtoAdd);
        Mockito.when(service.delete(1L)).thenReturn(breedDtoAdd);
    }

    @Test
    void find_all_breeds() {
        List<BreedDtoList> breedDto = service.findAll();
        assertEquals(2, breedDto.size());
        assertFalse(breedDto.isEmpty());
        assertNotNull(breedDto);
    }

    @Test
    void find_breed_by_id() {
        BreedDtoAdd breed = service.findById(1L);
        assertEquals(breed.getName(), breedDtoAdd.getName());
        assertNotNull(breed);
    }

    @Test
    void save_breed() {
        BreedDtoAdd save = service.save(breedDtoAdd);
        assertEquals(save.getName(), breedDtoAdd.getName());
        assertNotNull(save);
    }

    @Test
    void update_breed_by_id() {
        breedDtoAdd.setName("Angora");
        BreedDtoAdd edit = service.update(1L, breedDtoAdd);
        assertEquals(edit.getName(), breedDtoAdd.getName());
        assertNotNull(edit);
    }

    @Test
    void delete_breed_by_id() {
        breedDtoAdd.setStatus("inactivo");
        BreedDtoAdd drop = service.delete(1L);
        assertEquals("inactivo", drop.getStatus());
    }
}