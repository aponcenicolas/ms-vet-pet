package lux.vet.pet;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;



@SpringBootTest
class ServicePetApplicationTests {

    private final ServicePetApplication petApplication;

    @Autowired
    ServicePetApplicationTests(ServicePetApplication petApplication) {
        this.petApplication = petApplication;
    }

    @Test
    void sum() {
        Assertions.assertEquals(10, petApplication.sum(5, 5));
    }

}
