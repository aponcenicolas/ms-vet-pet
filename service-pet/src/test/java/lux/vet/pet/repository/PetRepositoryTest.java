package lux.vet.pet.repository;

import lux.vet.pet.model.entity.Pet;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;


@SpringBootTest
class PetRepositoryTest {

    private static final String ACTIVE = "activo";

    @Autowired
    private PetRepository repository;

    @Test
    void find_all_pets_by_status() {
        List<Pet> pets = repository.findByStatus(ACTIVE);
        assertNotNull(pets);
        assertTrue(pets.size() > 0);
    }

    @Test
    void find_pet_by_status_and_id() {
        Pet pet = repository.findByStatusAndId(ACTIVE, 1L);
        assertEquals(1L, pet.getId());
        assertNotNull(pet);
    }
}