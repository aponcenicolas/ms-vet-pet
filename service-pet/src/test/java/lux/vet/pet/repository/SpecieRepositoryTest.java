package lux.vet.pet.repository;

import lux.vet.pet.model.entity.Specie;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class SpecieRepositoryTest {

    private static final String ACTIVE = "activo";

    @Autowired
    private SpecieRepository repository;


    @Test
    void find_all_specie_by_status() {
        List<Specie> species = repository.findByStatus(ACTIVE);
        assertNotNull(species);
    }


    @Test
    void find_specie_by_status_and_id() {
        Specie specie = repository.findByStatusAndId(ACTIVE, 1L);
        assertNotNull(specie);
        assertEquals(1L, specie.getId());
    }
}