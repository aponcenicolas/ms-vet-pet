package lux.vet.pet.repository;

import lux.vet.pet.model.entity.Breed;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
class BreedRepositoryTest {

    private static final String ACTIVE = "activo";

    @Autowired
    private BreedRepository repository;

    @Test
    void find_all_breeds_by_status() {
        List<Breed> breeds = repository.findByStatus(ACTIVE);
        assertNotNull(breeds);
        assertTrue(breeds.size() > 0);
    }

    @Test
    void find_breed_by_status_and_id() {
        Breed breed = repository.findByStatusAndId(ACTIVE, 1L);
        assertEquals(1L, breed.getId());
        assertNotNull(breed);
    }
}