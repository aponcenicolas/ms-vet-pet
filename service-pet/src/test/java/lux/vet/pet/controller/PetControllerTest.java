package lux.vet.pet.controller;


import lux.vet.pet.implementation.PetServiceImpl;
import lux.vet.pet.model.dto.PetDtoAdd;
import lux.vet.pet.model.dto.PetDtoList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
class PetControllerTest {

    @InjectMocks
    private PetController controller;

    @Mock
    private PetServiceImpl service;

    public PetDtoAdd petDtoAdd;

    @BeforeEach
    void setUp() {

        List<PetDtoList> list = new ArrayList<>();
        list.add(PetDtoList.builder().id(1L).name("Tom").color("red").sex("male").specie("gato").breed("persa").status("activo").build());
        list.add(PetDtoList.builder().id(2L).name("Tim").color("red").sex("male").specie("perro").breed("pitbull").status("activo").build());

        petDtoAdd = PetDtoAdd.builder().id(1L).name("Tom").color("red").sex("male").specieId(1L).breedId(1L).status("active").build();

        Mockito.when(service.findAll()).thenReturn(list);
        Mockito.when(service.findById(1L)).thenReturn(petDtoAdd);
        Mockito.when(service.save(petDtoAdd)).thenReturn(petDtoAdd);
        Mockito.when(service.update(1L, petDtoAdd)).thenReturn(petDtoAdd);
        Mockito.when(service.delete(1L)).thenReturn(petDtoAdd);
    }

    @Test
    void find_all_pets() {
        List<PetDtoList> petDto = controller.findAll();
        assertEquals(2, petDto.size());
        assertFalse(petDto.isEmpty());
        assertNotNull(petDto);
    }

    @Test
    void find_pet_by_id() {
        PetDtoAdd pet = controller.findById(1L);
        assertEquals(pet.getName(), petDtoAdd.getName());
        assertNotNull(pet);
    }

    @Test
    void save_pet() {
        PetDtoAdd pet = controller.save(petDtoAdd);
        assertEquals(pet.getName(), petDtoAdd.getName());
        assertNotNull(pet);
    }

    @Test
    void update_pet() {
        petDtoAdd.setName("pepe");
        PetDtoAdd edit = controller.update(1L, petDtoAdd);
        assertEquals(edit.getName(), petDtoAdd.getName());
        assertNotNull(edit);
    }

    @Test
    void delete_pet() {
        petDtoAdd.setStatus("inactivo");
        PetDtoAdd pet = controller.delete(1L);
        assertEquals("inactivo", pet.getStatus());
    }
}