package lux.vet.pet.controller;


import lux.vet.pet.implementation.SpecieServiceImpl;
import lux.vet.pet.model.dto.SpecieDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
class SpecieControllerTest {

    @InjectMocks
    private SpecieController controller;

    @Mock
    private SpecieServiceImpl service;

    public SpecieDto specieDto;

    @BeforeEach
    void setUp() {
        List<SpecieDto> list = new ArrayList<>();
        list.add(SpecieDto.builder().id(1L).name("Gato").status("activo").build());
        list.add(SpecieDto.builder().id(2L).name("Perro").status("activo").build());
        list.add(SpecieDto.builder().id(3L).name("Conejo").status("activo").build());

        specieDto = SpecieDto.builder().id(1L).name("Gato").status("activo").build();

        Mockito.when(service.findAll()).thenReturn(list);
        Mockito.when(service.findById(1L)).thenReturn(specieDto);
        Mockito.when(service.save(specieDto)).thenReturn(specieDto);
        Mockito.when(service.update(1L, specieDto)).thenReturn(specieDto);
        Mockito.when(service.delete(1L)).thenReturn(specieDto);
    }

    @Test
    void findAll() {
        List<SpecieDto> specieDto = controller.findAll();
        assertEquals(3, specieDto.size());
        assertFalse(specieDto.isEmpty());
        assertNotNull(specieDto);
    }

    @Test
    void findById() {
        SpecieDto specie = controller.findById(1L);
        assertEquals(specie.getName(), specieDto.getName());
        assertNotNull(specie);
    }

    @Test
    void save() {
        SpecieDto save = controller.save(specieDto);
        assertEquals(save.getName(), specieDto.getName());
        assertNotNull(save);
    }

    @Test
    void update() {
        specieDto.setName("Perro");
        SpecieDto edit = controller.update(1L, specieDto);
        assertEquals(edit.getName(), specieDto.getName());
        assertNotNull(edit);
    }

    @Test
    void delete() {
        specieDto.setStatus("inactivo");
        SpecieDto drop = controller.delete(1L);
        assertEquals("inactivo", drop.getStatus());
    }
}