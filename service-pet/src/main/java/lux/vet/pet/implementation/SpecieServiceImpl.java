package lux.vet.pet.implementation;

import lux.vet.pet.model.dto.SpecieDto;
import lux.vet.pet.model.entity.Specie;
import lux.vet.pet.repository.SpecieRepository;
import lux.vet.pet.service.SpecieService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SpecieServiceImpl implements SpecieService {

    private static final String ACTIVE = "activo";
    private static final String INACTIVE = "inactivo";

    private final SpecieRepository repository;
    private final ModelMapper modelMapper;

    @Autowired
    public SpecieServiceImpl(SpecieRepository repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    private SpecieDto convertToDto(Specie specie) {
        return modelMapper.map(specie, SpecieDto.class);
    }

    private Specie convertToEntity(SpecieDto specieDto) {
        return modelMapper.map(specieDto, Specie.class);
    }

    @Override
    public List<SpecieDto> findAll() {
        List<Specie> species = repository.findByStatus(ACTIVE);
        return species.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @Override
    public SpecieDto findById(Long id) {
        return convertToDto(repository.findByStatusAndId(ACTIVE, id));
    }

    @Override
    public SpecieDto save(SpecieDto specieDto) {
        specieDto.setStatus(ACTIVE);
        Specie insert = repository.save(convertToEntity(specieDto));
        return convertToDto(insert);
    }

    @Override
    public SpecieDto update(Long id, SpecieDto specieDto) {
        Specie edit = repository.findByStatusAndId(ACTIVE, id);
        edit.setName(specieDto.getName());
        return convertToDto(repository.save(edit));
    }

    @Override
    public SpecieDto delete(Long id) {
        Specie drop = repository.findByStatusAndId(ACTIVE, id);
        drop.setStatus(INACTIVE);
        return convertToDto(repository.save(drop));
    }
}
