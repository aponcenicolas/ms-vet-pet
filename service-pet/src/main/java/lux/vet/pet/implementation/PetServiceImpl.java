package lux.vet.pet.implementation;

import lux.vet.pet.model.dto.PetDtoAdd;
import lux.vet.pet.model.dto.PetDtoList;
import lux.vet.pet.model.entity.Pet;
import lux.vet.pet.repository.PetRepository;
import lux.vet.pet.service.PetService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PetServiceImpl implements PetService {

    private static final String ACTIVE = "activo";
    private static final String INACTIVE = "inactivo";

    private final PetRepository repository;
    private final ModelMapper modelMapper;

    @Autowired
    public PetServiceImpl(PetRepository repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    private PetDtoList convertToDtoList(Pet pet) {
        return modelMapper.map(pet, PetDtoList.class);
    }

    private PetDtoAdd convertToDtoAdd(Pet pet) {
        return modelMapper.map(pet, PetDtoAdd.class);
    }

    private Pet convertToEntity(PetDtoAdd petDto) {
        return modelMapper.map(petDto, Pet.class);
    }

    @Override
    public List<PetDtoList> findAll() {
        List<Pet> pets = repository.findByStatus(ACTIVE);
        return pets.stream()
                .map(this::convertToDtoList)
                .collect(Collectors.toList());
    }

    @Override
    public PetDtoAdd findById(Long id) {
        return convertToDtoAdd(repository.findByStatusAndId(ACTIVE, id));
    }

    @Override
    public PetDtoAdd save(PetDtoAdd petDto) {
        petDto.setStatus(ACTIVE);
        Pet insert = repository.save(convertToEntity(petDto));
        return convertToDtoAdd(insert);
    }

    @Override
    public PetDtoAdd update(Long id, PetDtoAdd petDto) {
        PetDtoAdd edit = convertToDtoAdd(repository.findByStatusAndId(ACTIVE, id));
        edit.setName(petDto.getName());
        edit.setSex(petDto.getSex());
        edit.setColor(petDto.getColor());
        edit.setSpecieId(petDto.getSpecieId());
        edit.setBreedId(petDto.getBreedId());
        Pet pet = repository.save(convertToEntity(edit));
        return convertToDtoAdd(pet);
    }

    @Override
    public PetDtoAdd delete(Long id) {
        PetDtoAdd drop = convertToDtoAdd(repository.findByStatusAndId(ACTIVE, id));
        drop.setStatus(INACTIVE);
        Pet pet = repository.save(convertToEntity(drop));
        return convertToDtoAdd(pet);
    }
}
