package lux.vet.pet.implementation;

import lux.vet.pet.model.dto.BreedDtoList;
import lux.vet.pet.model.dto.BreedDtoAdd;
import lux.vet.pet.model.entity.Breed;
import lux.vet.pet.repository.BreedRepository;
import lux.vet.pet.service.BreedService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BreedServiceImpl implements BreedService {

    private static final String ACTIVE = "activo";
    private static final String INACTIVE = "inactivo";

    private final BreedRepository repository;
    private final ModelMapper modelMapper;


    @Autowired
    public BreedServiceImpl(BreedRepository repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    private BreedDtoList convertToDtoList(Breed breed) {
        return modelMapper.map(breed, BreedDtoList.class);
    }

    private BreedDtoAdd convertToDtoAdd(Breed breed) {
        return modelMapper.map(breed, BreedDtoAdd.class);
    }

    private Breed convertToEntity(BreedDtoAdd breedDto) {
        return modelMapper.map(breedDto, Breed.class);
    }

    @Override
    public List<BreedDtoList> findAll() {
        List<Breed> breeds = repository.findByStatus(ACTIVE);
        return breeds.stream()
                .map(this::convertToDtoList)
                .collect(Collectors.toList());
    }


    @Override
    public BreedDtoAdd findById(Long id) {
        return convertToDtoAdd(repository.findByStatusAndId(ACTIVE, id));
    }

    @Override
    public BreedDtoAdd save(BreedDtoAdd breedDto) {
        breedDto.setStatus(ACTIVE);
        Breed insert = repository.save(convertToEntity(breedDto));
        return convertToDtoAdd(insert);
    }

    @Override
    public BreedDtoAdd update(Long id, BreedDtoAdd breedDto) {
        BreedDtoAdd edit = convertToDtoAdd( repository.findByStatusAndId(ACTIVE, id));
        edit.setName(breedDto.getName());
        edit.setSpecieId(breedDto.getSpecieId());
        Breed breed=repository.save(convertToEntity(edit));
        return convertToDtoAdd(breed);
    }

    @Override
    public BreedDtoAdd delete(Long id) {
        BreedDtoAdd drop = convertToDtoAdd( repository.findByStatusAndId(ACTIVE, id));
        drop.setStatus(INACTIVE);
        Breed breed=repository.save(convertToEntity(drop));
        return convertToDtoAdd(breed);
    }
}
