package lux.vet.pet.repository;


import lux.vet.pet.model.entity.Breed;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BreedRepository extends JpaRepository<Breed, Long> {
    List<Breed> findByStatus(String status);

    Breed findByStatusAndId(String status, Long id);
}
