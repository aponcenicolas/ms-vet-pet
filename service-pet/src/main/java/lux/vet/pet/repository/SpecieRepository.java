package lux.vet.pet.repository;

import lux.vet.pet.model.entity.Specie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpecieRepository extends JpaRepository<Specie, Long> {
    List<Specie> findByStatus(String status);

    Specie findByStatusAndId(String status, Long id);
}
