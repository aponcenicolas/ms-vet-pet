package lux.vet.pet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ServicePetApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServicePetApplication.class, args);
    }

    public int sum(int x, int y) {
        return x + y;
    }

}
