package lux.vet.pet.controller;

import lux.vet.pet.model.dto.SpecieDto;
import lux.vet.pet.service.SpecieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/species")
public class SpecieController {

    private final SpecieService service;

    @Autowired
    public SpecieController(SpecieService service) {
        this.service = service;
    }

    @GetMapping
    public List<SpecieDto> findAll() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public SpecieDto findById(@PathVariable Long id) {
        return service.findById(id);
    }

    @PostMapping
    public SpecieDto save(@RequestBody SpecieDto specieDto) {
        return service.save(specieDto);
    }

    @PutMapping("/{id}")
    public SpecieDto update(@PathVariable Long id, @RequestBody SpecieDto specieDto) {
        return service.update(id, specieDto);
    }

    @DeleteMapping("/{id}")
    public SpecieDto delete(@PathVariable Long id) {
        return service.delete(id);
    }
}
