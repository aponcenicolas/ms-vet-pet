package lux.vet.pet.controller;

import lux.vet.pet.model.dto.BreedDtoList;
import lux.vet.pet.model.dto.BreedDtoAdd;
import lux.vet.pet.service.BreedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/breeds")
public class BreedController {

    private final BreedService service;

    @Autowired
    public BreedController(BreedService service) {
        this.service = service;
    }

    @GetMapping
    public List<BreedDtoList> findAll() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public BreedDtoAdd findById(@PathVariable Long id) {
        return service.findById(id);
    }

    @PostMapping
    public BreedDtoAdd save(@RequestBody BreedDtoAdd breedDto) {
        return service.save(breedDto);
    }

    @PutMapping("/{id}")
    public BreedDtoAdd update(@PathVariable Long id, @RequestBody BreedDtoAdd breedDto) {
        return service.update(id, breedDto);
    }

    @DeleteMapping("/{id}")
    public BreedDtoAdd delete(@PathVariable Long id) {
        return service.delete(id);
    }
}
