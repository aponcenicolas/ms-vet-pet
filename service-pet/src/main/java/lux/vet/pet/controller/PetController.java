package lux.vet.pet.controller;

import lux.vet.pet.model.dto.PetDtoAdd;
import lux.vet.pet.model.dto.PetDtoList;
import lux.vet.pet.service.PetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pets")
public class PetController {

    private final PetService service;

    @Autowired
    public PetController(PetService service) {
        this.service = service;
    }

    @GetMapping
    public List<PetDtoList> findAll() {
        return service.findAll();
    }

    @GetMapping("/{id}")
    public PetDtoAdd findById(@PathVariable Long id) {
        return service.findById(id);
    }

    @PostMapping
    public PetDtoAdd save(@RequestBody PetDtoAdd petDto) {
        return service.save(petDto);
    }

    @PutMapping("/{id}")
    public PetDtoAdd update(@PathVariable Long id, @RequestBody PetDtoAdd petDto) {
        return service.update(id, petDto);
    }

    @DeleteMapping("/{id}")
    public PetDtoAdd delete(@PathVariable Long id) {
        return service.delete(id);
    }
}
