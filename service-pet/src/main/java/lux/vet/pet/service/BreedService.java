package lux.vet.pet.service;

import lux.vet.pet.model.dto.BreedDtoAdd;
import lux.vet.pet.model.dto.BreedDtoList;

import java.util.List;

public interface BreedService {
    List<BreedDtoList> findAll();

    BreedDtoAdd findById(Long id);

    BreedDtoAdd save(BreedDtoAdd breedDto);

    BreedDtoAdd update(Long id, BreedDtoAdd breedDto);

     BreedDtoAdd delete(Long id);
}
