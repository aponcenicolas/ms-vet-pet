package lux.vet.pet.service;

import lux.vet.pet.model.dto.PetDtoAdd;
import lux.vet.pet.model.dto.PetDtoList;

import java.util.List;

public interface PetService {
    List<PetDtoList> findAll();

    PetDtoAdd findById(Long id);

    PetDtoAdd save(PetDtoAdd petDto);

    PetDtoAdd update(Long id, PetDtoAdd petDto);

    PetDtoAdd delete(Long id);
}
