package lux.vet.pet.service;

import lux.vet.pet.model.dto.SpecieDto;

import java.util.List;

public interface SpecieService {
    List<SpecieDto> findAll();

    SpecieDto findById(Long id);

    SpecieDto save(SpecieDto specieDto);

    SpecieDto update(Long id, SpecieDto specieDto);

    SpecieDto delete(Long id);
}
