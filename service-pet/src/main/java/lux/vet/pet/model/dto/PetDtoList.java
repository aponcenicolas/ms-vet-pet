package lux.vet.pet.model.dto;

import lombok.*;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PetDtoList {

    private Long id;
    private String name;
    private String sex;
    private String color;
    private String status;
    private String specie;
    private String breed;
}
