package lux.vet.pet.model.dto;

import lombok.*;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SpecieDto {

    private Long id;
    private String name;
    private String status;
}
