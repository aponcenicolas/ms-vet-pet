package lux.vet.pet.model.dto;


import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PetDtoAdd {

    private Long id;
    private String name;
    private String sex;
    private String color;
    private String status;
    private Long specieId;
    private Long breedId;
}
